#! /usr/bin/env python
"""Tuxy Bot install script."""

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


setup(
    name="tuxy-bot",
    version="0.1.0",
    description="Implementation for tuxy-bot challenge",
    long_description=open("README.md").read(),
    author="Wansome - Python Seria 1",
    url="https://github.com/wantsome/python/seria1/tuxy_bot",
    packages=["tuxy_bot", "tuxy_bot.client", "tuxy_bot.common"],
    entry_points={
        'console_scripts': [
            'tuxy_bot = tuxy_bot.cmdline:main',
        ],
    },
    requires=open("requirements.txt").readlines(),
)
