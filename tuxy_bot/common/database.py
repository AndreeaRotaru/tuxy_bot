"""Persistent dictionary-like object."""

import json
import logging
import os
import tempfile

LOG = logging.getLogger(__name__)
LOG.addHandler(logging.StreamHandler())


class _Database(object):
    """Contract class for all the Database objects."""
    
    def __getitem__(self, key):
        return self._get_item(key)

    def __setitem__(self, key, value):
        self._set_item(key, value)

    def _get_item(self, key):
        pass

    def _set_item(self, key, value):
        pass

    def exists(self, key):
        """Check if key exists in internal database."""
        if self._get_item(key):
            return True
        return False

    def save(self):
        """Save the current database to file."""
        pass


class Database(_Database):

    """Persistent dictionary-like object."""

    __database_instance = None

    def __init__(self, filename=None):
        if filename is None:
            tempdir = tempfile.gettempdir()
            filename = os.path.join(tempdir, 'database')

        self._database = {}
        self._file = os.path.abspath(filename)
        self._load()

    def __new__(cls):
        if not cls.__database_instance:
            cls.__database_instance = super(Database, cls).__new__(cls)
        return cls.__database_instance

    def _get_item(self, key):
        return self._database.get(key)

    def _set_item(self, key, value):
        self._database[key] = value

    def _load(self):
        if os.path.isfile(self._file):
            with open(self._file, 'r') as database:
                content = database.read()
                try:
                    self._database = json.loads(content)
                except (ValueError, TypeError) as exc:
                    LOG.error('Nu am putut citi baza de date: %r', exc)

    def get(self, key, default=None):
        """Get the value for the received key."""
        return self._database.get(key, default)

    def keys(self):
        """Get all the keys from database."""
        for key in self._database:
            yield key

    def values(self):
        """Get all the values from the database."""
        for key in self._database:
            yield self._database[key]

    def save(self):
        """Save the current database to file."""
        with open(self._file, 'w') as database:
            json.dump(self._database, database)

    def clean(self):
        """Remove all the items from database."""
        self._database.clean()
