#!/usr/bin/env python3
"""Tuxy-Bot entry point."""

import argparse
import sys

from tuxy_bot import cli
from tuxy_bot.client import item, math, string, crypto


class TuxyBotClient(cli.Application):

    """TuxyBotClient command line application."""

    commands = [
        (item.Item, 'commands'),
        (math.Math, 'commands'),
        (string.String, 'commands'),
        (crypto.Crypto, 'commands')
    ]

    def setup(self):
        """Extend the parser configuration in order to expose all
        the received commands.
        """
        self._parser = argparse.ArgumentParser()
        commands = self._parser.add_subparsers(
            title="[commands]", dest="command")

        self._register_parser("commands", commands)


def main():
    """Run the Tuxy-Bot command line application."""
    tuxy_bot = TuxyBotClient(sys.argv[1:])
    tuxy_bot.run()


if __name__ == "__main__":
    main()
