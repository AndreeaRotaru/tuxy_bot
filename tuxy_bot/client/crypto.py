from tuxy_bot import cli

class Crypto(cli.Group):
    """ Commands for encrypting or decrypting messages. """

    commands = []

    def setup(self):
        """Extend the parser configuration in order to expose this command."""
        parser = self._parser.add_parser(
            "crypto", help="Gives acces to cryptography commands.")

        actions = parser.add_subparsers()
        self._register_parser("actions", actions)