from tuxy_bot import cli

class _Palindrom(cli.Command):

    def __init__(self, parent, parser):
        super(_Palindrom, self).__init__(parent, parser)

    def setup(self):
        """Extend the parser configuration in order to expose this command."""
        parser = self._parser.add_parser("palindrom", help="Checks if a string is palindrome.")
        parser.add_argument('key', help="The name of the string you want to check.")
        parser.set_defaults(work=self.run)

    def _work(self):
    	cuvant = self.args.key
    	if cuvant == cuvant[::-1]:
    	    print(cuvant, "este un palindrom")
    	else:
    	    print(cuvant, "nu este un palindrom")
    	return cuvant

class String(cli.Group):
    """Group for all the available item actions."""

    commands = [(_Palindrom, "actions")]

    def setup(self):
        """Extend the parser configuration in order to expose this command."""
        parser = self._parser.add_parser(
            "string", help="Different string commands. 'Fun with strings':)")

        actions = parser.add_subparsers()
        self._register_parser("actions", actions)
