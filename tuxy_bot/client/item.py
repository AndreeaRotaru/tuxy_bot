"""Command line options for the item section."""
from __future__ import print_function

from tuxy_bot import cli
from tuxy_bot import exception
from tuxy_bot.common import database



class _Add(cli.Command):
  
    def __init__(self, parent, parser):
        super(_Add, self).__init__(parent, parser)
        self._database = database.Database()

    def setup(self):
        """Extend the parser configuration in order to expose this command."""
        parser = self._parser.add_parser(
            "add", help="Save the item in the internal database.")
        parser.add_argument("key", type=str, help="The name of the item.")
        parser.add_argument("value", type=str, help="The value of the current item.")
        parser.set_defaults(work=self.run)

    def _work(self):
        self._database[self.args.key] = self.args.value
        self._database.save()


class _Get(cli.Command):
    
    def __init__(self, parent, parser):
        super(_Get, self).__init__(parent, parser)
        self._database = database.Database()

    def setup(self):
        """Extend the parser configuration in order to expose this command."""
        parser = self._parser.add_parser("get", help="Get the value for one item.")
        parser.add_argument('key', help="The name of the required item.")
        parser.set_defaults(work=self.run)

    def _work(self):
        if not self._database.exists(self.args.key):
            raise exception.NotFound(object=self.args.key,
                                     container="internal database.")
        valoare = self._database[self.args.key]
        
        # FIXME: Nu este o abordare dorita
        print(valoare)
        return valoare
		
class _List(cli.Command):
    
    def __init__(self, parent, parser):
        super(_List, self).__init__(parent, parser)
        self._database = database.Database()

    def setup(self):
        """Extend the parser configuration in order to expose this command."""
        parser = self._parser.add_parser("list", help="List the first 5 keys and values in alphabetical order.")
       # parser.add_argument('key', help="The name of the required item.")
        parser.set_defaults(work=self.run)

    def _work(self):
        """Add all keys from database in a list, order them and list first five elements"""
        list_of_keys = list(self._database.keys())
        list_of_values = list(self._database.values())
                
        n = len(list_of_keys)
        for i in range(n):
            for j in range(0, n-i-1):
                if list_of_values[j] > list_of_values[j+1]:
                    list_of_values[j], list_of_values[j+1] = list_of_values[j+1], list_of_values[j]
                    list_of_keys[j], list_of_keys[j+1] = list_of_keys[j+1], list_of_keys[j]

        for i in range(5):    
            print('{} : {}'.format(list_of_keys[i],list_of_values[i]))      
              
        
class Item(cli.Group):
    """Group for all the available item actions."""

    commands = [(_Add, "actions"), (_Get, "actions"), (_List, "actions")]

    def setup(self):
        """Extend the parser configuration in order to expose this command."""
        parser = self._parser.add_parser(
            "item", help="Save items into internal database.")

        actions = parser.add_subparsers()
        self._register_parser("actions", actions)
