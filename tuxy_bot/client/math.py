"""Command line options for the math section."""
from __future__ import print_function
from tuxy_bot import cli


class _Prime(cli.Command):
    """It checks whether the number is prime or not"""

    def setup(self):
        """Extend the parser configuration in order to expose this command."""
        parser = self._parser.add_parser(
            "prime", help="Integer is prime.")
        parser.add_argument("integer", type=int, help= "The value of the item.")
        parser.set_defaults(work=self.run)

    def _work(self):
        i = 0
        divisor = 1
        while divisor <= self.args.integer:
            if self.args.integer % divisor == 0:
                i = i + 1
                divisor = divisor + 1
            else:
                divisor = divisor + 1
        if i == 2:
            print (self.args.integer, "is prime")
        else:
            print (self.args.integer, "is not prime")

class _Unic(cli.Command):
    """ This class finds the unic elements of a list """

    def setup(self):
        """Extend the parser configuration in order to expose this command."""
        parser = self._parser.add_parser(
            "unic", help="Finds the unic elements of a list")
        parser.add_argument('integers', metavar='N', type=int, nargs='+',
                            help='an integer for the accumulator')

        parser.set_defaults(work=self.run)

    def _work(self):

        unic = self.args.integers.pop()
        for element in self.args.integers:
            unic ^= element
        print(unic)


class _Sum(cli.Command):
    """It sums the provided numbers"""
    def setup(self):
        """Extend the parser configuration in order to expose this command."""
        parser = self._parser.add_parser(
            "sum", help= "The sum of the integers.")
    
        parser.add_argument("integers", nargs = "+", type=int, help= "The value of the item.")
        parser.set_defaults(work=self.run)
    
    def _work(self):
        
        print(sum(self.args.integers))


class _Produs(cli.Command):
    """This class calculates produs between two numbers."""
    def __init__(self, parent, parser):
        super(_Produs, self).__init__(parent, parser)

    def setup(self):
        """Extend the parser configuration in order to expose this command."""
        parser = self._parser.add_parser(
            "multiply", help="Multiply two numbers.")
        parser.add_argument("number_one", type=int, help="First number.")
        parser.add_argument("number_two", type=int, help="Second number.")
        parser.set_defaults(work=self.run)

    def _work(self):

         print(self.args.number_one * self.args.number_two)

class _Diferenta(cli.Command):
    """This class calculates difference between two numbers."""

    def __init__(self, parent, parser):
        super(_Diferenta, self).__init__(parent, parser)

    def setup(self):
        """Extend the parser configuration in order to expose this command."""
        parser = self._parser.add_parser(
            "diff", help="Subtracts two numbers.")
        parser.add_argument("number_one", type=int, help="First number.")
        parser.add_argument("number_two", type=int, help="Second number.")

        parser.set_defaults(work=self.run)

    def _work(self):
         print(self.args.number_one - self.args.number_two)

class _Putere(cli.Command):
    """This class calculates x**y (x to the power of y)"""
    def __init__(self, parent, parser):
        super(_Putere, self).__init__(parent, parser)

    def setup(self):
        """Return x**y (x to the power of y)"""
        parser = self._parser.add_parser(
                "power", help="x to the power of y")
        parser.add_argument("number_one", type=int, help="First number.")
        parser.add_argument("number_two", type=int, help="Second number.")

        parser.set_defaults(work=self.run)

    def _work(self):
        print(self.args.number_one ** self.args.number_two)

class Math(cli.Group):
    """It provides access to the mathematical functions"""

    commands = [(_Produs, 'actions'), (_Diferenta, 'actions'), (_Prime, "actions"), (_Putere, "actions"), (_Sum, "actions"), (_Unic, 'actions')]


    def setup(self):
        """Extend the parser configuration in order to expose this command."""
        parser = self._parser.add_parser(
            "math", help="Execute mathematical operations")

        actions = parser.add_subparsers()
        self._register_parser("actions", actions)
